# customwidgetgenerator

[Repository](https://gitlab.com/lucanerlich/customwidgetgenerator)

This project works on a given dictionary of xtypes and their corresponding implementation location.
Based on these - and on pre written file templates - it generates a conversion rule for the "Dialog Conversion Tool".

See: https://opensource.adobe.com/aem-modernize-tools/pages/tools/dialog.html

[E-Mail](nerlich@adobe.com)
