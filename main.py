# -----------------------------------------------------------
# This project works on a given dictionary of xtypes and their corresponding implementation location.
# Based on these - and on pre written file templates - it generates a conversion rule for the "Dialog Conversion Tool".
# See: https://opensource.adobe.com/aem-modernize-tools/pages/tools/dialog.html
# email nerlich@adobe.com
# -----------------------------------------------------------

import os

from constants.dct import patterncontent, rulecontent
from constants.widgetinclude import widgetinclude
from constants.xtypepaths import xtypepaths

PLACEHOLDER_XTYPE = "{{xtype}}"
PLACEHOLDER_NAME = "{{widgetname}}"
PLACEHOLDER_WIDGETPATH = "{{path}}"
PLACEHOLDER_LABEL = "{{label}}"


def get_widget_name(path):
    """
    Returns the Widgets Name for the given path.
    :param path: Xtype Widget Path -> /apps/myif/master/commons/widgets/admin/core/js/ComponentDialog.js
    :return: FileName without extension -> ComponentDialog
    """
    return path.split("/")[-1:][0].split(".js")[0]


def get_widget_path(path):
    """
    Returns the Widgets Path without the file extension.
    :param path: Xtype Widget Path -> /apps/myif/master/commons/widgets/admin/core/js/ComponentDialog.js
    :return:  /apps/myif/master/commons/widgets/admin/core/js/ComponentDialog
    """
    return path.split(".js")[0]


def create_dirs(maindir, patternsdir):
    """
    :param maindir: DCT Rule Path
    :param patternsdir: DCT Rule Patterns Path
    """
    if not os.path.exists(maindir):
        os.makedirs(maindir)
    if not os.path.exists(patternsdir):
        os.makedirs(patternsdir)


def create_rules():
    """
    Generates a conversion rule for each entry in the given xtype/path dictionary.
    """
    for key in xtypepaths:
        xtype = key
        widget_name = get_widget_name(xtypepaths[xtype]).lower()
        maindir = "./output/dct/" + widget_name
        patternsdir = maindir + "/patterns"
        widget_path = get_widget_path(xtypepaths[xtype]).lower()

        create_dirs(maindir, patternsdir)

        # Uncomment the following lines, to trigger the file creation

        # create_widget_includes(widget_name, widget_path)
        # create_rule_content(maindir, widget_name, widget_path)
        # create_rule_pattern(patternsdir, widget_name, xtype)


def create_widget_includes(widgetname, widgetpath):
    f = open("./output/widgetIncludeOutput.xml", "a")
    f.write("\n")
    filecontent = widgetinclude
    filecontent = filecontent.replace(PLACEHOLDER_NAME, widgetname)
    filecontent = filecontent.replace(PLACEHOLDER_WIDGETPATH, widgetpath)
    filecontent = filecontent.replace(PLACEHOLDER_LABEL, widgetpath)
    f.write(filecontent)
    f.close()


def create_rule_content(filepath, widgetname, widgetpath):
    """
    Creates the rules .content.xml
    :param filepath: DCT Rule Path
    :param widgetname: Custom Widget Name
    :param widgetpath: Custom Widget Path
    """
    f = open(filepath + "/.content.xml", "w")
    filecontent = rulecontent.rulecontent
    filecontent = filecontent.replace(PLACEHOLDER_NAME, widgetname)
    filecontent = filecontent.replace(PLACEHOLDER_WIDGETPATH, widgetpath)
    f.write(filecontent)
    f.close()


def create_rule_pattern(filepath, widgetname, xtype):
    """
    Creates the rules pattern.xml
    :param filepath: DCT Rule Path
    :param widgetname: Custom Widget Name
    :param xtype: ClassicUI Xtype
    """
    f = open(filepath + "/" + widgetname + ".xml", "w")
    filecontent = patterncontent.patterncontent
    f.write(filecontent.replace(PLACEHOLDER_XTYPE, xtype))
    f.close()


if __name__ == '__main__':
    create_rules()
