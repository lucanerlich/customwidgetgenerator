xtypepaths = {
    "admin.componentdialog": "/apps/myif/master/commons/widgets/admin/core/js/ComponentDialog.js",
    "admin.dialog.portlet": "/apps/myif/master/commons/widgets/admin/core/js/dialog/Portlet.js",
    "admin.mdi.anlaesseCheckbox": "/apps/myif/master/commons/widgets/admin/core/js/selection/AnlaesseCheckbox.js",
    "admin.selection.chatTags": "/apps/myif/master/commons/widgets/admin/core/js/selection/ChatTagsSelection.js",
    "admin.selection.deviceSelection": "/apps/myif/master/commons/widgets/admin/core/js/selection/DeviceSelection.js",
    "admin.selection.gvStatusSelection": "/apps/myif/master/commons/widgets/admin/core/js/selection/GvStatusSelection.js",
    "admin.selection.produkt": "/apps/myif/master/commons/widgets/admin/core/js/selection/ProduktSelection.js",
    "admin.selection.statusVisibility": "/apps/myif/master/commons/widgets/admin/core/js/selection/StatusVisibility.js",
    "admin.selection.suchradius": "/apps/myif/master/commons/widgets/admin/core/js/selection/SuchradiusSelection.js",
    "beraterkarussell.angezeigteKontaktdatenPanel": "/apps/myif/zentral/fi/beraterdaten/components/beraterkarussell/clientlib/js/angezeigteKontaktdatenPanel.js",
    "beraterkarussell.beschraenkteSelection": "/apps/myif/zentral/fi/beraterdaten/components/beraterkarussell/clientlib/js/beschraenkteSelection.js",
    "beraterkarussell.beschraenkteText": "/apps/myif/zentral/fi/beraterdaten/components/beraterkarussell/clientlib/js/beschraenkteText.js",
    "beraterkarussell.fileField": "/apps/myif/zentral/fi/beraterdaten/components/contact/kontaktBoxIFA/clientlib/js/fileField.js",
    "beraterkarussell.tagsselection": "/apps/myif/zentral/fi/beraterdaten/components/beraterkarussell/clientlib/js/tags_selection.js",
    "beraterkarussell.validierteBeschraenkteText": "/apps/myif/zentral/fi/beraterdaten/components/beraterkarussell/clientlib/js/validierteBeschraenkteText.js",
    "commonfields.betrag": "/apps/myif/master/commons/widgets/admin/common_fields/js/Betrag.js",
    "commonfields.ganzzahl": "/apps/myif/master/commons/widgets/admin/common_fields/js/Ganzzahl.js",
    "commonfields.zinssatz": "/apps/myif/master/commons/widgets/admin/common_fields/js/Zinssatz.js",
    "ifa.dialog.field.datum": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/componentdialoge/dialogDatum.js",
    "ifa.dialog.field.nummer": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/componentdialoge/dialogNummer.js",
    "ifa.dialog.field.zahl": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/componentdialoge/dialogZahl.js",
    "ifa.feldgruppe.dialog": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/feldgruppedialoge/fgDialog.js",
    "ifa.feldgruppe.dialog.anzeigebedingung": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/feldgruppedialoge/fgAnzeigebedingungen.js",
    "ifa.feldgruppe.dialog.propertiesBestaetigung": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/feldgruppedialoge/fgBestaetigung.js",
    "ifa.feldgruppe.dialog.referenz": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/feldgruppedialoge/fgReferenz.js",
    "ifa.feldgruppe.dialog.texte": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/feldgruppedialoge/fgTexte.js",
    "ifa.feldgruppe.vordef.dialog.propertiesBestaetigung": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/feldgruppedialoge/fgBestaetigungVordef.js",
    "ifa.field.dialog.anzeigebedingung": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/felddialoge/feldAnzeigebedingungen.js",
    "ifa.field.dialog.datum": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/felddialoge/feldDatum.js",
    "ifa.field.dialog.eigenschaften": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/felddialoge/feldEigenschaften.js",
    "ifa.field.dialog.eigenschaftenAbsendenButton": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/felddialoge/feldEigenschaftenAbsendenButton.js",
    "ifa.field.dialog.eigenschaftenBild": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/felddialoge/feldEigenschaftenBild.js",
    "ifa.field.dialog.eigenschaftenFesterTextBest": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/felddialoge/feldEigenschaftenFesterTextBest.js",
    "ifa.field.dialog.texte": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/felddialoge/feldTexte.js",
    "ifa.field.dialog.texteOSP": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/felddialoge/feldTexteOSP.js",
    "ifa.field.dialog.verwendung": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/felddialoge/feldVerwendung.js",
    "ifa.ifalink": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/utils/ifalink.js",
    "ifa.page.dialog.weiterKonfigurationBest": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/seitedialoge/seiteWeiterKonfigurationBest.js",
    "ifa.page.dialog.weiterKonfigurationFallabschlussBest": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/seitedialoge/seiteWeiterKonfigurationFallabschlussBest.js",
    "ifa.page.dialog.weiterKonfigurationFallabschlussWV": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/seitedialoge/seiteWeiterKonfigurationFallabschlussWV.js",
    "ifa.page.dialog.weiterKonfigurationWV": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/seitedialoge/seiteWeiterKonfigurationWV.js",
    "ifa.seite.dialog.anzeigebedingung": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/seitedialoge/seiteAnzeigebedingungen.js",
    "ifa.seite.dialog.texte": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/seitedialoge/seiteTexte.js",
    "ifa.seite.dialog.texteKurz": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/seitedialoge/seiteTexteKurz.js",
    "ifa.seite.dialog.variantenwechsel": "/apps/myif/master/ifauftrag/widgets/admin/ifa/js/seitedialoge/seiteVariantenwechsel.js",
    "ifep.authoring.dialog": "/apps/myif/master/elementepool/widgets/admin/ifep-authoring/js/dialog.js",
    "modul.assets_selector": "/apps/modulfabrik/moduldefinition/widgets/moduldefinition/js/AssetsSelector.js",
    "modul.components_selector": "/apps/modulfabrik/moduldefinition/widgets/moduldefinition/js/ComponentsSelector.js",
    "modul.designs_selector": "/apps/modulfabrik/moduldefinition/widgets/moduldefinition/js/DesignsSelector.js",
    "modul.pages_selector": "/apps/modulfabrik/moduldefinition/widgets/moduldefinition/js/PagesSelector.js",
    "neo.feldgruppe.dialog": "/apps/myif/master/neo/widgets/admin/neo/js/feldgruppedialoge/fgDialog.js",
    "neo.feldgruppe.dialog.anzeigebedingung": "/apps/myif/master/neo/widgets/admin/neo/js/feldgruppedialoge/fgAnzeigebedingungen.js",
    "neo.feldgruppe.dialog.referenz": "/apps/myif/master/neo/widgets/admin/neo/js/feldgruppedialoge/fgReferenz.js",
    "neo.feldgruppe.dialog.texte": "/apps/myif/master/neo/widgets/admin/neo/js/feldgruppedialoge/fgTexte.js",
    "neo.field.dialog.anzeigebedingung": "/apps/myif/master/neo/widgets/admin/neo/js/felddialoge/feldAnzeigebedingungen.js",
    "neo.field.dialog.eigenschaften": "/apps/myif/master/neo/widgets/admin/neo/js/felddialoge/feldEigenschaften.js",
    "neo.field.dialog.eigenschaftenBild": "/apps/myif/master/neo/widgets/admin/neo/js/felddialoge/feldEigenschaftenBild.js",
    "neo.field.dialog.texte": "/apps/myif/master/neo/widgets/admin/neo/js/felddialoge/feldTexte.js",
    "neo.field.dialog.verwendung": "/apps/myif/master/neo/widgets/admin/neo/js/felddialoge/feldVerwendung.js",
    "neo.seite.dialog.texteKurz": "/apps/myif/master/neo/widgets/admin/neo/js/seitedialoge/seiteTexteKurz.js",
    "stresstest.lebenshaltungskostenCustomMultifield": "/apps/myif/master/commons/widgets/admin/common_fields/js/LebenshaltungskostenMultiField.js",
    "ui.filefield": "/apps/myif/master/commons/widgets/admin/core/js/FileField.js",
    "ui.form.FilialeDamPathField": "/apps/myif/master/widgets/js/FilialeDamPathField.js",
    "ui.form.FilialePathField": "/apps/myif/master/widgets/js/FilialePathField.js",
    "ui.iconselection": "/apps/myif/master/commons/widgets/admin/core/js/Iconpicker.js",
    "ui.linkfieldbox": "/apps/myif/master/commons/widgets/admin/core/js/LinkFieldBox.js",
    "ui.linkfieldset": "/apps/myif/master/commons/widgets/admin/core/js/LinkField.js",
    "ui.linkfieldseturltype": "/apps/myif/master/commons/widgets/admin/core/js/LinkFieldUrlType.js",
}
